{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = github:nixos/nixpkgs/nixos-23.05;

    impermanence.url = github:nix-community/impermanence;
  };

  outputs = { self, nixpkgs, ... }@inputs:
    let
      system = "x86_64-linux";
    in
      {
        nixosConfigurations = {
          nixos = nixpkgs.lib.nixosSystem {
            inherit system;
            modules = [
              ./configuration.nix
              ./hardware-configuration.nix
            ];
            specialArgs = { inherit inputs; };
          };
        };
      };
}
