# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, lib, inputs, ... }:
let
  # Only enable auto upgrade if current config came from a clean tree
  # This avoids accidental auto-upgrades when working locally.
  # Credits to https://git.sr.ht/~misterio
  isClean = inputs.self ? rev;
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      inputs.impermanence.nixosModules.impermanence
    ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;

  services.zfs.autoScrub.enable = true;
  networking.hostId = "00000000";

  # Warning: this erases everything inside root which is not bind mounted
  boot.initrd.postDeviceCommands = lib.mkAfter ''
    zfs rollback -r rpool/local/root@blank
  '';

  networking.hostName = "nixos";
  networking.networkmanager.enable = true;

  time.timeZone = "Europe/Helsinki";
  system.autoUpgrade = {
    enable = isClean;
    dates = "hourly";
    flags = [ "--refresh" ];
    flake = "gitlab:niklashh/nixos-luks-zfs-example";
  };

  environment.persistence = {
    "/persist" = {
      directories = [
        { directory = "/var/lib/systemd"; mode = "u=rwx,g=rx,o=rx"; }
        { directory = "/var/lib/nixos"; mode = "u=rwx,g=rx,o=rx"; }
        { directory = "/var/log"; mode = "u=rwx,g=rx,o=rx"; }
      ];
      files = [
        "/etc/machine-id"
        "/etc/ssh/ssh_host_rsa_key"
        "/etc/ssh/ssh_host_rsa_key.pub"
        "/etc/ssh/ssh_host_ed25519_key"
        "/etc/ssh/ssh_host_ed25519_key.pub"
      ];
    };
  };

  i18n.defaultLocale = "en_US.UTF-8";
  console.useXkbConfig = true;
  services.xserver.layout = "us";
  services.xserver.xkbVariant = "altgr-intl";

  # FIXME: This password can't be changed with `passwd` (due to impermanence of /etc/shadow). Use a proper secret management solution.
  users.users.root.initialPassword = "toor";

  services.openssh = {
    enable = true;
    hostKeys = [
      {
        path = "/persist/ssh/ssh_host_ed25519_key";
        type = "ed25519";
      }
      {
        path = "/persist/ssh/ssh_host_rsa_key";
        type = "rsa";
        bits = 4096;
      }
    ];
  };

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  system.stateVersion = "23.05";
}
